using UnityEngine;
using System.Collections;

///
/// !!! Machine generated code !!!
/// !!! DO NOT CHANGE Tabs to Spaces !!!
/// 
[System.Serializable]
public class Sheet1Data
{
  [SerializeField]
  string jobname;
  public string Jobname { get {return jobname; } set { jobname = value;} }
  
  [SerializeField]
  string size;
  public string Size { get {return size; } set { size = value;} }
  
  [SerializeField]
  string mainlocations;
  public string Mainlocations { get {return mainlocations; } set { mainlocations = value;} }
  
  [SerializeField]
  string industries;
  public string Industries { get {return industries; } set { industries = value;} }
  
  [SerializeField]
  int weeklyearnings;
  public int Weeklyearnings { get {return weeklyearnings; } set { weeklyearnings = value;} }
  
  [SerializeField]
  string ftsize;
  public string Ftsize { get {return ftsize; } set { ftsize = value;} }
  
  [SerializeField]
  int ftpercentage;
  public int Ftpercentage { get {return ftpercentage; } set { ftpercentage = value;} }
  
  [SerializeField]
  int hours;
  public int Hours { get {return hours; } set { hours = value;} }
  
  [SerializeField]
  int age;
  public int Age { get {return age; } set { age = value;} }
  
  [SerializeField]
  int femalepercentages;
  public int Femalepercentages { get {return femalepercentages; } set { femalepercentages = value;} }
  
  [SerializeField]
  string futuregrowth;
  public string Futuregrowth { get {return futuregrowth; } set { futuregrowth = value;} }
  
  [SerializeField]
  string skilllevel;
  public string Skilllevel { get {return skilllevel; } set { skilllevel = value;} }
  
}