using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum ETargetStage
{
    None,
    TAFE,
    University,
    Work
}

public class PlayerData : UnitySingleton<PlayerData>
{
    [SerializeField] private int year10to12Score;
    [SerializeField] private int adjustScore;
    [SerializeField] private int tafeScore;
    [SerializeField] private int universityScore;
    [SerializeField] private int workScore;
    [SerializeField] private int tppScore;
    [SerializeField] private ECareer career;
    [SerializeField] private string careerName;
    [SerializeField] private ETargetStage targetStage;

    public int Year10to12Score { get => year10to12Score; set => year10to12Score = value; }
    public int TafeScore { get => tafeScore; set => tafeScore = value; }
    public int UniversityScore { get => universityScore; set => universityScore = value; }
    public int WorkScore { get => workScore; set => workScore = value; }
    public int TppScore { get => tppScore; set => tppScore = value; }
    public ECareer Career { get => career; set => career = value; }
    public int AdjustScore { get => adjustScore; set => adjustScore = value; }
    public ETargetStage TargetStage { get => targetStage; set => targetStage = value; }
    public string CareerName { get => careerName; set => careerName = value; }
}
