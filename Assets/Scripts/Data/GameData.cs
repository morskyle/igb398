using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum Pathways
{
    year10to12,
    tafe,
    university,
    work,
    tpp
}

public class GameData : UnitySingleton<GameData>
{
    private int enrollmentScore = 30;

    public int EnrollmentScore { get => enrollmentScore; set => enrollmentScore = value; }
}
