using System.Collections;
using System.Collections.Generic;
using UnityEngine;
public enum ECareer
{
    Accountant,
    Actors,
    Authors,
    AgriculturalOperators,
    AmbulanceOfficer,
    Bakers,
    Bankers,
    Broker,
    BusDriver,
    BuildingLabourers,
    Biologists,
    CafeWorkers,
    CarSalesperson,
    Chefs,
    ChemicalEngineer,
    ChildCare,
    DeliveryDrivers,
    Dentists,
    DeveloperProgrammers,
    Directors,
    ElectricalEngineers,
    EnrolledNurses,
    FashionDesigners,
    FinanceManagers,
    GameDevelopers,
    HighSchoolTeacher,
    Hairdresser,
    Interpreters,
    LandscapeArchitects,
    MiningEngineers,
    Miners,
    Physiotherapists,
    Pilots,
    SoftwareEngineers,
    Storepersons,
    SchoolTeachers,
    UniversityLecturers,
    WebDevelopers,
    WelfareWorkers,
}
public class CareerData : UnitySingleton<CareerData>
{
    public Dictionary<int, ECareer> IntToECareer = new Dictionary<int, ECareer>();
    public Dictionary<ECareer, string> ECareerToString = new Dictionary<ECareer, string>();
    
    public List<Sheet1Data> careerData;

    // Start is called before the first frame update
    void Start()
    {
        CareerDataPool.InitialiseData();
        careerData = CareerDataPool.careerDT.dataList;


       

        InitialiseIntToECareerDictionary();

        InitialiseECareerToStringDictionary();
    }

    private void InitialiseIntToECareerDictionary()
    {
        IntToECareer.Add(0, ECareer.Accountant);
        IntToECareer.Add(1, ECareer.Actors);
        IntToECareer.Add(2, ECareer.Authors);
        IntToECareer.Add(3, ECareer.AgriculturalOperators);
        IntToECareer.Add(4, ECareer.AmbulanceOfficer);
        IntToECareer.Add(5, ECareer.Bakers);
        IntToECareer.Add(6, ECareer.Bankers);
        IntToECareer.Add(7, ECareer.Broker);
        IntToECareer.Add(8, ECareer.BusDriver);
        IntToECareer.Add(9, ECareer.BuildingLabourers);
        IntToECareer.Add(10, ECareer.Biologists);
        IntToECareer.Add(11, ECareer.CafeWorkers);
        IntToECareer.Add(12, ECareer.CarSalesperson);
        IntToECareer.Add(13, ECareer.Chefs);
        IntToECareer.Add(14, ECareer.ChemicalEngineer);
        IntToECareer.Add(15, ECareer.ChildCare);
        IntToECareer.Add(16, ECareer.DeliveryDrivers);
        IntToECareer.Add(17, ECareer.Dentists);
        IntToECareer.Add(18, ECareer.DeveloperProgrammers);
        IntToECareer.Add(19, ECareer.Directors);
        IntToECareer.Add(20, ECareer.ElectricalEngineers);
        IntToECareer.Add(21, ECareer.EnrolledNurses);
        IntToECareer.Add(22, ECareer.FashionDesigners);
        IntToECareer.Add(23, ECareer.FinanceManagers);
        IntToECareer.Add(24, ECareer.GameDevelopers);
        IntToECareer.Add(25, ECareer.HighSchoolTeacher);
        IntToECareer.Add(26, ECareer.Hairdresser);
        IntToECareer.Add(27, ECareer.Interpreters);
        IntToECareer.Add(28, ECareer.LandscapeArchitects);
        IntToECareer.Add(29, ECareer.MiningEngineers);
        IntToECareer.Add(30, ECareer.Miners);
        IntToECareer.Add(31, ECareer.Physiotherapists);
        IntToECareer.Add(32, ECareer.Pilots);
        IntToECareer.Add(33, ECareer.SoftwareEngineers);
        IntToECareer.Add(34, ECareer.Storepersons);
        IntToECareer.Add(35, ECareer.SchoolTeachers);
        IntToECareer.Add(36, ECareer.UniversityLecturers);
        IntToECareer.Add(37, ECareer.WebDevelopers);
        IntToECareer.Add(38, ECareer.WelfareWorkers);
    }
    private void InitialiseECareerToStringDictionary()
    {
        
        ECareerToString.Add(ECareer.Accountant, "Accountant");
        ECareerToString.Add(ECareer.Actors, "Actors");
        ECareerToString.Add(ECareer.Authors, "Authors");
        ECareerToString.Add(ECareer.AgriculturalOperators, "Agricultural Operators");
        ECareerToString.Add(ECareer.AmbulanceOfficer, "Ambulance Officer");
        ECareerToString.Add(ECareer.Bakers, "Bakers");
        ECareerToString.Add(ECareer.Bankers, "Bankers");
        ECareerToString.Add(ECareer.Broker, "Broker");
        ECareerToString.Add(ECareer.BusDriver, "Bus Driver");
        ECareerToString.Add(ECareer.BuildingLabourers, "Building Labourers");
        ECareerToString.Add(ECareer.Biologists, "Biologists");
        ECareerToString.Add(ECareer.CafeWorkers, "Cafe Workers");
        ECareerToString.Add(ECareer.CarSalesperson, "Car Salesperson");
        ECareerToString.Add(ECareer.Chefs, "Chefs");
        ECareerToString.Add(ECareer.ChemicalEngineer, "Chemical Engineer");
        ECareerToString.Add(ECareer.ChildCare, "Child Care");
        ECareerToString.Add(ECareer.DeliveryDrivers, "Delivery Drivers");
        ECareerToString.Add(ECareer.Dentists, "Dentists");
        ECareerToString.Add(ECareer.DeveloperProgrammers, "Developer Programmers");
        ECareerToString.Add(ECareer.Directors, "Directors");
        ECareerToString.Add(ECareer.ElectricalEngineers, "Electrical Engineers");
        ECareerToString.Add(ECareer.EnrolledNurses, "Enrolled Nurses");
        ECareerToString.Add(ECareer.FashionDesigners, "Fashion Designers");
        ECareerToString.Add(ECareer.FinanceManagers, "Finance Managers");
        ECareerToString.Add(ECareer.GameDevelopers, "Game Developers");
        ECareerToString.Add(ECareer.HighSchoolTeacher, "High School Teacher");
        ECareerToString.Add(ECareer.Hairdresser, "Hairdresser");
        ECareerToString.Add(ECareer.Interpreters, "Interpreters");
        ECareerToString.Add(ECareer.LandscapeArchitects, "Landscape Architects");
        ECareerToString.Add(ECareer.MiningEngineers, "Mining Engineers");
        ECareerToString.Add(ECareer.Miners, "Miners");
        ECareerToString.Add(ECareer.Physiotherapists, "Physiotherapists");
        ECareerToString.Add(ECareer.Pilots, "Pilots");
        ECareerToString.Add(ECareer.SoftwareEngineers, "Software Engineers");
        ECareerToString.Add(ECareer.Storepersons, "Storepersons");
        ECareerToString.Add(ECareer.SchoolTeachers, "School Teachers");
        ECareerToString.Add(ECareer.UniversityLecturers, "University Lecturers");
        ECareerToString.Add(ECareer.WebDevelopers, "Web Developers");
        ECareerToString.Add(ECareer.WelfareWorkers, "Welfare Workers");
    }

    /// <summary>
    /// Initialise data
    /// </summary>
    
}
