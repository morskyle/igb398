using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum EDoor : int
{
    door_CareerChoice_North,
    door_Year1012_North,
    door_Pathway_University,
    door_Pathway_TAFE,
    door_Pathway_Work,
    door_TAFE_South,
    door_TAFE_North,
    door_TPP_South,
    door_TPP_East,
    door_University_South,
    door_University_West,
    door_University_East,
    door_Work_South,
    door_Work_North,
    door_CareerSummary_South,
    door_CareerSummary_West
}
public class DoorManager : UnitySingleton<DoorManager>
{
    public GameObject currentDoor;
    public GameObject door_CareerChoice_North;
    public GameObject door_Year1012_North;        
    public GameObject door_Pathway_University;     
    public GameObject door_Pathway_TAFE;      
    public GameObject door_Pathway_Work;      
    public GameObject door_TAFE_South;
    public GameObject door_TAFE_North;
    public GameObject door_TPP_South;
    public GameObject door_TPP_East;
    public GameObject door_University_South;
    public GameObject door_University_West;
    public GameObject door_University_East;
    public GameObject door_Work_South;
    public GameObject door_Work_North;
    public GameObject door_CareerSummary_South;
    public GameObject door_CareerSummary_West;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void OpenDoor(EDoor door)
    {
        switch (door)
        {
            case EDoor.door_CareerChoice_North:
                door_CareerChoice_North.SetActive(true);
                break;

            case EDoor.door_Year1012_North:
                door_Year1012_North.SetActive(true);
                break;

            case EDoor.door_Pathway_University:
                door_Pathway_University.SetActive(true);
                break;

            case EDoor.door_Pathway_TAFE:
                door_Pathway_TAFE.SetActive(true);
                break;

            case EDoor.door_Pathway_Work:
                door_Pathway_Work.SetActive(true);
                break;

            case EDoor.door_TAFE_South:
                door_TAFE_South.SetActive(true);
                break;

            case EDoor.door_TAFE_North:
                door_TAFE_North.SetActive(true);
                break;

            case EDoor.door_TPP_South:
                door_TPP_South.SetActive(true);
                break;

            case EDoor.door_TPP_East:
                door_TPP_East.SetActive(true);
                break;

            case EDoor.door_University_South:
                door_University_South.SetActive(true);
                break;

            case EDoor.door_University_West:
                door_University_West.SetActive(true);
                break;

            case EDoor.door_University_East:
                door_University_East.SetActive(true);
                break;

            case EDoor.door_Work_South:
                door_Work_South.SetActive(true);
                break;

            case EDoor.door_Work_North:
                door_Work_North.SetActive(true);
                break;

            case EDoor.door_CareerSummary_South:
                door_CareerSummary_South.SetActive(true);
                break;

            case EDoor.door_CareerSummary_West:
                door_CareerSummary_West.SetActive(true);
                break;

            default:
                break;
        }
    }

    public void CloseDoor(EDoor door)
    {
        switch (door)
        {
            case EDoor.door_CareerChoice_North:
                door_CareerChoice_North.SetActive(false);
                break;

            case EDoor.door_Year1012_North:
                door_Year1012_North.SetActive(false);
                break;

            case EDoor.door_Pathway_University:
                door_Pathway_University.SetActive(false);
                break;

            case EDoor.door_Pathway_TAFE:
                door_Pathway_TAFE.SetActive(false);
                break;

            case EDoor.door_Pathway_Work:
                door_Pathway_Work.SetActive(false);
                break;

            case EDoor.door_TAFE_South:
                door_TAFE_South.SetActive(false);
                break;

            case EDoor.door_TAFE_North:
                door_TAFE_North.SetActive(false);
                break;

            case EDoor.door_TPP_South:
                door_TPP_South.SetActive(false);
                break;

            case EDoor.door_TPP_East:
                door_TPP_East.SetActive(false);
                break;

            case EDoor.door_University_South:
                door_University_South.SetActive(false);
                break;

            case EDoor.door_University_West:
                door_University_West.SetActive(false);
                break;

            case EDoor.door_University_East:
                door_University_East.SetActive(false);
                break;

            case EDoor.door_Work_South:
                door_Work_South.SetActive(false);
                break;

            case EDoor.door_Work_North:
                door_Work_North.SetActive(false);
                break;

            case EDoor.door_CareerSummary_South:
                door_CareerSummary_South.SetActive(false);
                break;

            case EDoor.door_CareerSummary_West:
                door_CareerSummary_West.SetActive(false);
                break;

            default:
                break;
        }
    }
}
