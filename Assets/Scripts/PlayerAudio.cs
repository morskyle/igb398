using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerAudio : UnitySingleton<PlayerAudio>
{
    public AudioSource sourceA;
    public AudioSource sourceB;
    public AudioSource sourceC;
    public AudioSource BGM;
    public AudioClip BGM_01;
    public AudioClip BGM_02;
    public AudioClip SE_GuidePointTrigger;
    public AudioClip SE_ButtonClicked;
    public AudioClip SE_PageFlip;
    public AudioClip SE_DoorOpen;
    public AudioClip SE_WindowsOpen;
    public AudioClip SE_Hammer;
    public AudioClip SE_Scholarship_Coin;
    public AudioClip SE_BubbleSpawn;
    public AudioClip SE_Bubble_Benefit;
    public AudioClip SE_Bubble_Bad;
    public AudioClip SE_CorrectOrder;
    public AudioClip SE_IncorrectOrder;
    public AudioClip SE_Timer;
    public AudioClip SE_TimesUp;

    public void SetSourceAClip(AudioClip clip)
    {
        sourceA.clip = clip;
    }
    public void SetSourceBClip(AudioClip clip)
    {
        sourceB.clip = clip;
    }
    public void SetSourceCClip(AudioClip clip)
    {
        sourceC.clip = clip;
    }
    public void SetBGM(AudioClip clip)
    {
        BGM.clip = clip;
    }
    public void PlaySourceA()
    {
        sourceA.Play();
    }
    public void PlaySourceB()
    {
        sourceB.Play();
    }
    public void PlaySourceC()
    {
        sourceC.Play();
    }

    public void PlayBGM()
    {
        BGM.Play();
    }

    public void PlaySourceA(AudioClip clip)
    {
        SetSourceAClip(clip);
        sourceA.Play();
    }
    public void PlaySourceB(AudioClip clip)
    {
        SetSourceBClip(clip);
        sourceB.Play();
    }
    public void PlaySourceC(AudioClip clip)
    {
        SetSourceCClip(clip);
        sourceC.Play();
    }

    public void PlayBGM(AudioClip clip)
    {
        SetBGM(clip);
        BGM.Play();
    }

    public void PauseBGM()
    {
        BGM.Pause();
    }

    public void SetSEVolume(float v)
    {
        sourceA.volume = v;
        sourceB.volume = v;
        sourceC.volume = v;
    }

    public void SetBGMVolume(float v)
    {
        BGM.volume = v;
    }
}
