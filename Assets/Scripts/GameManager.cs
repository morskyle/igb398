using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public enum EGameStep : int
{ 
    CareerChoice,
    Year1012,
    PathwayPassage,
    TAFE,
    University,
    Work,
    TPP,
    CareerSummary
}

public class GameManager : UnitySingleton<GameManager>
{
    public GameObject ingameMenu;
    public Text gameState;
    public Text txtDebug;
    public bool unlimitedGame = false;
    public bool playedFlipBook = false;
    public bool isGaming = false;

    private EGameStep currentStep;
    public EGameStep CurrentStep { get => currentStep; set => currentStep = value; }

    void Start()
    {
        //[DEBUG]txtDebug.text = "";
        PlayerAudio.Instance.PlayBGM(PlayerAudio.Instance.BGM_01);
        CurrentStep = EGameStep.CareerChoice;
       
    }

    // Update is called once per frame
    void Update()
    {
        if (OVRInput.Get(OVRInput.Button.Start))
        {
            //ingameMenu.SetActive(!ingameMenu.activeSelf);
            ingameMenu.SetActive(true);
        }
    }

    public void SetGameStep(EGameStep step)
    {
        CurrentStep = step;
    }

    public EGameStep GetCurrentGameStep()
    {
        return CurrentStep;
    }


}
