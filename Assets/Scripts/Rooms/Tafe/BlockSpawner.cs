using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BlockSpawner : MonoBehaviour
{
    public GameObject block;
    //public bool endSpawn = false;
    public IEnumerator spawner;

    // Start is called before the first frame update
    void Start()
    {
        spawner = Spawner();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void StartSpawn()
    {
        StartCoroutine(spawner);
    }

    public void StopSpawn()
    {
        StopCoroutine(spawner);
    }

    private IEnumerator Spawner()
    {
        while (true)
        {
            yield return new WaitForSeconds(Random.Range(5, 10));
            TAFEGameController.Instance.AddBlockIntoList(Instantiate(block, transform.position, new Quaternion(0, 0, 90, 1)));
        }


    }
}
