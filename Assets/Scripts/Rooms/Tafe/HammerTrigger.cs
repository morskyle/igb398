using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HammerTrigger : MonoBehaviour
{
    public GameObject introduction;
    [SerializeField] private float offset; 
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Hammer")
        {
            introduction.SetActive(true);

            PlayerAudio.Instance.PlaySourceA(PlayerAudio.Instance.SE_Hammer);
            //TAFEGameController.Instance.StartGame();

            ////[Temporary] 
            this.gameObject.SetActive(false);
        }
    }

    public void ResetTrigger()
    {

    }


}
