using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SlideBlock : MonoBehaviour
{
    private Rigidbody r;
    public bool canDestroy;
    public float moveSpeed = 4f;
    
    // Start is called before the first frame update
    void Start()
    {
        canDestroy = false;
        r = GetComponent<Rigidbody>();
}

    // Update is called once per frame
    void Update()
    {
        Vector3 v = transform.position + Vector3.forward.normalized * -moveSpeed * Time.deltaTime;
        r.MovePosition(v);
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Platform_TAFE")
        {
            canDestroy = true;
        }

        if (other.tag == "Hammer")
        {
            if (canDestroy)
            {
                PlayerAudio.Instance.PlaySourceA(PlayerAudio.Instance.SE_Hammer);
                Destroy(gameObject);
                TAFEGameController.Instance.AddScore(1);
                TAFEGameController.Instance.RemoveBlockFromList(this.gameObject);
            }

        }
  
    }

    private void OnTriggerExit(Collider other)
    {

        if (other.tag == "Platform_TAFE")
        {
            Destroy(gameObject, 4f);
        }
    }
}
