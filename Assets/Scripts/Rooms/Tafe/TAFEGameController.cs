using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class TAFEGameController : UnitySingleton<TAFEGameController>
{
    [SerializeField]private int score = 0;
    public BlockSpawner[] spawners;
    public List<GameObject> slideBlocks;
    public Text txtScore;
    public GameObject introduction;
    public GameObject choiceBoardTAFE;

    // Start is called before the first frame update
    void Start()
    {
        ShowScore();
    }

    //Show game score by UI.Text
    private void ShowScore()
    {
        txtScore.text = "" + score;
    }

    //Record game score in PlayerData
    private void RecordScore()
    {
        PlayerData.Instance.TafeScore = score;
    }

    private void OpenChoiceBoard()
    {
        choiceBoardTAFE.SetActive(true);
    }

    //Add slide block into list
    public void AddBlockIntoList(GameObject b)
    {
        slideBlocks.Add(b);
    }
    
    //Remove slide block from list
    public void RemoveBlockFromList(GameObject b)
    {
        slideBlocks.Remove(b);
    }

    //Clean all blocks
    public void RemoveAllBlocks()
    {
        foreach (GameObject slideBlock in slideBlocks)
        {
            Destroy(slideBlock);
        }
    }

    //Add score
    public void AddScore(int s)
    {
        score += s;
        ShowScore();
    }

    //Start game
    public void StartGame()
    {
        //Debug.Log("Start");
        PlayerAudio.Instance.PauseBGM();
        foreach (BlockSpawner spawner in spawners)
        {
            spawner.StartSpawn();
        }

        introduction.SetActive(false);

        ////Start to time
        TimerManager.Instance.tafeTimer.StartTimer();
    }

    //Stop game
    public void StopGame()
    {
        Invoke("ContinueBGM", 1f);
        foreach (BlockSpawner spawner in spawners)
        {
            spawner.StopSpawn();
        }

        RemoveAllBlocks();
        
    }
   

    public void AfterGame()
    {
        RecordScore();

        OpenChoiceBoard();
    }
    //Reset game
    public void ResetGame()
    {
        //reset timer
        //reset trigger
    }

    private void ContinueBGM()
    {
        PlayerAudio.Instance.PlayBGM();
    }
}
