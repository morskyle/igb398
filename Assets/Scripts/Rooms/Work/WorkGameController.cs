using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public enum EOrder : int
{
    Dish_Cheesecake_Strawberry,
    Dish_Cheesecake_Blueberry,
    Dish_Cheesecake_Chocolatte,
    Dish_Cheesecake_Lime,
    Drinks_Espresso_Cup,
    Drinks_Tea_Cup
}
public class WorkGameController : UnitySingleton<WorkGameController>
{
    [SerializeField] private int score = 0;
    public GameObject screen;
    public GameObject[] Orders;
    public EOrder currentOrder;
    public GameObject startTrigger;
    public GameObject touchTrigger;

    public Text txtScore;
    public GameObject nextPoint;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void StartGame()
    {
        PlayerAudio.Instance.PauseBGM();
        TimerManager.Instance.workTimer.StartTimer();
        RandomOrder();
        startTrigger.SetActive(false);
        touchTrigger.SetActive(true);
    }

    public void StopGame()
    {
        Invoke("ContinueBGM", 1f);
        touchTrigger.SetActive(false);
        RecordScore();
        DisplayNextPoint();
    }

   

    public void IncreaseScore(int s)
    {
        score += s;

        DiplayScore();
    }

    private void DiplayScore()
    {
        txtScore.text = "" + score;
    }

    public void Replenishment(EOrder order)
    {
        //GameObject go = Instantiate(Orders[(int)order]);
        GameObject go = Instantiate(Orders[0]);
    }

    public int RandomOrder()
    {
        int orderIndex = Random.Range(0, 6);

        switch (orderIndex)
        {
            case 0:
                currentOrder = EOrder.Dish_Cheesecake_Strawberry;
                break;
            case 1:
                currentOrder = EOrder.Dish_Cheesecake_Blueberry;
                break;
            case 2:
                currentOrder = EOrder.Dish_Cheesecake_Chocolatte;
                break;
            case 3:
                currentOrder = EOrder.Dish_Cheesecake_Lime;
                break;
            case 4:
                currentOrder = EOrder.Drinks_Espresso_Cup;
                break;
            case 5:
                currentOrder = EOrder.Drinks_Tea_Cup;
                break;
            default:
                break;
        }

        screen.GetComponent<OrderScreen>().DisplayOrderPicture(currentOrder);

        return orderIndex;
    }

    public void RecordScore()
    {
        PlayerData.Instance.WorkScore = score;
    }

    public void DisplayNextPoint()
    {
        nextPoint.SetActive(true);
    }

    private void ContinueBGM()
    {
        PlayerAudio.Instance.PlayBGM();
    }
}
