using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TouchTrigger_TakeOrder : MonoBehaviour
{
    public GameObject wrongOrder;
    [SerializeField] private EOrder order;
    [SerializeField] private GameObject go;
    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Order")
        {
            go = other.gameObject;
            order = other.gameObject.GetComponent<Orders>().orderType;
            other.gameObject.GetComponent<OVRGrabbable>().GrabEnd(Vector3.zero, Vector3.zero);
            //Match order's type with current order's type in screen
            if (order == WorkGameController.Instance.currentOrder)
            {
                PlayerAudio.Instance.PlaySourceA(PlayerAudio.Instance.SE_CorrectOrder);
                //Increase score
                WorkGameController.Instance.IncreaseScore(other.gameObject.GetComponent<Orders>().score);
                //New Order
                WorkGameController.Instance.RandomOrder();
            }
            else
            {
                PlayerAudio.Instance.PlaySourceA(PlayerAudio.Instance.SE_IncorrectOrder);
                //Display Message(wrong order)
                wrongOrder.SetActive(true);
            }

            other.gameObject.SetActive(false);

            //Replenishment(order);
            //Destroy Order gameobject
            //Destroy(other.gameObject);


            Invoke("ResetOrder", 1f);
            //Hide Message(wrong order)
            Invoke("HideMessage", 2f);

            //StartCoroutine(Replenishment(order.orderType));
        }
    }

    public void ResetOrder()
    {
        
        go.transform.position = go.GetComponent<Orders>().originPos.position;
        go.transform.rotation = go.GetComponent<Orders>().originPos.rotation;
        go.SetActive(true);
    }

    public void HideMessage()
    {
        wrongOrder.SetActive(false);
    }

    public void Replenishment(EOrder order)
    {
        WorkGameController.Instance.Replenishment(order);
    }

}
