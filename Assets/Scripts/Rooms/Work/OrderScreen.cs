using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class OrderScreen : MonoBehaviour
{
    public Sprite[] orderPictures;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void DisplayOrderPicture(EOrder order)
    {
        this.gameObject.GetComponent<Image>().sprite = orderPictures[(int)order];
    }

}
