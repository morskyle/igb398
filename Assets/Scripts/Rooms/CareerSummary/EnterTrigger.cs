using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnterTrigger : MonoBehaviour
{
    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {
            PlayerAudio.Instance.PlayBGM(PlayerAudio.Instance.BGM_02);
        }
    }
}
