using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class CareerSummaryContent : UnitySingleton<CareerSummaryContent>
{
    [SerializeField] private Text leftContent;
    [SerializeField] private Text midContent;
    [SerializeField] private Text rightContent;
    [SerializeField] private Image educationLevel;

    public Sprite[] pictures;

    // Start is called before the first frame update
    void Start()
    {
       
;
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void Display()
    {
        DisplayLeftContent();
        DisplayMidContent();
        DisplayRightContent();
        DisplayEducationLevel();
    }

    private void DisplayLeftContent()
    {
        leftContent.text = "Skill Level: The average skill level rating of " + PlayerData.Instance.CareerName + " is " + CareerData.Instance.careerData.Find(name => name.Jobname == PlayerData.Instance.CareerName).Skilllevel + ".";
    }

    private void DisplayMidContent()
    {
        midContent.text =
            "Thanks for finishing the game and welcome to the Career Summary.\n" +
            "<color=blue><b>Caution</b></color>: The Australian jobs market is changing in response to the COVID-19 pandemic. These estimates do not take account of the impact of COVID-19. They may not reflect the current job market and should be used and interpreted with extreme caution.\n\n" +
            "Your ideal career is " + PlayerData.Instance.CareerName + "\n\n" +
            "<color=blue><b>Size</b></color>: This is a " + CareerData.Instance.careerData.Find(name => name.Jobname == PlayerData.Instance.CareerName).Size + " occupation.\n" +
            "<color=blue><b>Location</b></color>: " + PlayerData.Instance.CareerName + " work in many parts of Australia. " + CareerData.Instance.careerData.Find(name => name.Jobname == PlayerData.Instance.CareerName).Mainlocations + " have/has a large share of workers.\n" +
            "<color=blue><b>Industries</b></color>: Most work in " + CareerData.Instance.careerData.Find(name => name.Jobname == PlayerData.Instance.CareerName).Industries + "\n";
            
    }

    private void DisplayRightContent()
    {
        rightContent.text =
            "<color=blue><b>Earnings</b></color>: Full-time workers on an adult wage earn around $" + CareerData.Instance.careerData.Find(name => name.Jobname == PlayerData.Instance.CareerName).Weeklyearnings + " per week (higher than the average of $1,460). Earnings tend to be lower when starting out and higher as experience grows.\n" +
            "<color=blue><b>Full-time</b></color>: " + CareerData.Instance.careerData.Find(name => name.Jobname == PlayerData.Instance.CareerName).Ftsize + " work full-time (" + CareerData.Instance.careerData.Find(name => name.Jobname == PlayerData.Instance.CareerName).Ftpercentage + "% higher/less than the average of 66%). \n" +
            "<color=blue><b>Hours</b></color>: Full-time workers spend around " + CareerData.Instance.careerData.Find(name => name.Jobname == PlayerData.Instance.CareerName).Hours + " hours per week at work(compared to the average of 44 hours). \n" +
            "<color=blue><b>Age</b></color>: The average age is " + CareerData.Instance.careerData.Find(name => name.Jobname == PlayerData.Instance.CareerName).Age + " years(compared to the average of 40 years). \n" +
            "<color=blue><b>Gender</b></color>: " + CareerData.Instance.careerData.Find(name => name.Jobname == PlayerData.Instance.CareerName).Femalepercentages + "% of workers are female(compared to the average of 48%).\n" +
            "<color=blue><b>Future Growth</b></color>: The number of workers is expected to grow " + CareerData.Instance.careerData.Find(name => name.Jobname == PlayerData.Instance.CareerName).Futuregrowth + " over the next five years (2021-2025).";
    }

    private void DisplayEducationLevel()
    {
        switch (PlayerData.Instance.Career)
        {
            case ECareer.Accountant:
                educationLevel.sprite = pictures[0];
                break;
            case ECareer.Actors:
                educationLevel.sprite = pictures[1];
                break;
            case ECareer.Authors:
                educationLevel.sprite = pictures[2];
                break;
            case ECareer.AgriculturalOperators:
                educationLevel.sprite = pictures[3];
                break;
            case ECareer.AmbulanceOfficer:
                educationLevel.sprite = pictures[4];
                break;
            case ECareer.Bakers:
                educationLevel.sprite = pictures[5];
                break;
            case ECareer.Bankers:
                educationLevel.sprite = pictures[6];
                break;
            case ECareer.Broker:
                educationLevel.sprite = pictures[7];
                break;
            case ECareer.BusDriver:
                educationLevel.sprite = pictures[8];
                break;
            case ECareer.BuildingLabourers:
                educationLevel.sprite = pictures[9];
                break;
            case ECareer.Biologists:
                educationLevel.sprite = pictures[10];
                break;
            case ECareer.CafeWorkers:
                educationLevel.sprite = pictures[11];
                break;
            case ECareer.CarSalesperson:
                educationLevel.sprite = pictures[12];
                break;
            case ECareer.Chefs:
                educationLevel.sprite = pictures[13];
                break;
            case ECareer.ChemicalEngineer:
                educationLevel.sprite = pictures[14];
                break;
            case ECareer.ChildCare:
                educationLevel.sprite = pictures[15];
                break;
            case ECareer.DeliveryDrivers:
                educationLevel.sprite = pictures[16];
                break;
            case ECareer.Dentists:
                educationLevel.sprite = pictures[17];
                break;
            case ECareer.DeveloperProgrammers:
                educationLevel.sprite = pictures[18];
                break;
            case ECareer.Directors:
                educationLevel.sprite = pictures[19];
                break;
            case ECareer.ElectricalEngineers:
                educationLevel.sprite = pictures[20];
                break;
            case ECareer.EnrolledNurses:
                educationLevel.sprite = pictures[21];
                break;
            case ECareer.FashionDesigners:
                educationLevel.sprite = pictures[22];
                break;
            case ECareer.FinanceManagers:
                educationLevel.sprite = pictures[23];
                break;
            case ECareer.GameDevelopers:
                educationLevel.sprite = pictures[24];
                break;
            case ECareer.HighSchoolTeacher:
                educationLevel.sprite = pictures[25];
                break;
            case ECareer.Hairdresser:
                educationLevel.sprite = pictures[26];
                break;
            case ECareer.Interpreters:
                educationLevel.sprite = pictures[27];
                break;
            case ECareer.LandscapeArchitects:
                educationLevel.sprite = pictures[28];
                break;
            case ECareer.MiningEngineers:
                educationLevel.sprite = pictures[29];
                break;
            case ECareer.Miners:
                educationLevel.sprite = pictures[30];
                break;
            case ECareer.Physiotherapists:
                educationLevel.sprite = pictures[31];
                break;
            case ECareer.Pilots:
                educationLevel.sprite = pictures[32];
                break;
            case ECareer.SoftwareEngineers:
                educationLevel.sprite = pictures[33];
                break;
            case ECareer.Storepersons:
                educationLevel.sprite = pictures[34];
                break;
            case ECareer.SchoolTeachers:
                educationLevel.sprite = pictures[35];
                break;
            case ECareer.UniversityLecturers:
                educationLevel.sprite = pictures[36];
                break;
            case ECareer.WebDevelopers:
                educationLevel.sprite = pictures[37];
                break;
            case ECareer.WelfareWorkers:
                educationLevel.sprite = pictures[38];
                break;
            default:
                break;
        }
    }
}
