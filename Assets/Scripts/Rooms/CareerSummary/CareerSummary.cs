using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CareerSummary : MonoBehaviour
{
    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {
            CareerSummaryContent.Instance.Display();
            PlayerAudio.Instance.PlaySourceA(PlayerAudio.Instance.SE_GuidePointTrigger);
            this.gameObject.SetActive(false);
        }
    }
}
