using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public enum eRankAdjust
{
    subject,
    athlete,
    difficulty,
    none
}

public class RankAdjustController : UnitySingleton<RankAdjustController>
{
    public GameObject introduction;
   
    public GameObject subject;
    public GameObject athlete;
    public GameObject difficulty;
    
    public Button btnFinancial;
    public Button btnEnvironment;
    public Button btnDisability;
    public Button btnDisrupted;

    [SerializeField]private eRankAdjust curRankAdjustStep;

    // Start is called before the first frame update
    void Start()
    {
        curRankAdjustStep = eRankAdjust.subject;
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void DisplayForm(eRankAdjust rankAdjust)
    {
        switch (rankAdjust)
        {
            case eRankAdjust.subject:
                subject.SetActive(true); 
                break;

            case eRankAdjust.athlete:
                athlete.SetActive(true);
                break;

            case eRankAdjust.difficulty:
                difficulty.SetActive(true);
                break;
            default:
                break;
        }
    }

    public void CloseForm(eRankAdjust rankAdjust)
    {
        switch (rankAdjust)
        {
            case eRankAdjust.subject:
                subject.SetActive(false);
                break;

            case eRankAdjust.athlete:
                athlete.SetActive(false);
                break;

            case eRankAdjust.difficulty:
                difficulty.SetActive(false);
                break;
            default:
                break;
        }
    }

    public void StartIntroduction()
    {
        PlayerAudio.Instance.PlaySourceA(PlayerAudio.Instance.SE_WindowsOpen);
        introduction.SetActive(true);
    }

    public void StartRankAdjust()
    {
        PlayerAudio.Instance.PlaySourceA(PlayerAudio.Instance.SE_WindowsOpen);
        introduction.SetActive(false);
        DisplayForm(eRankAdjust.subject);
        curRankAdjustStep = eRankAdjust.subject;
    }

    public void ButtonYes()
    {
        PlayerAudio.Instance.PlaySourceB(PlayerAudio.Instance.SE_ButtonClicked);
        switch (curRankAdjustStep)
        {
            case eRankAdjust.subject:

                //Rank adjust
                AdjustScore(4);
                //Forms display and close
                AfterSubject();

                break;

            case eRankAdjust.athlete:

                //Rank adjust
                AdjustScore(4);
                //Forms display and close
                AfterAthlete();

                break;

            //case eRankAdjust.difficulty:

            //    //Rank adjust
            //    AdjustScore(4);
            //    //Forms display and close
            //    AfterDifficulty();
            //    break;

            case eRankAdjust.none:
                break;
            default:
                break;
        }
    }

    public void ButtonNo()
    {
        PlayerAudio.Instance.PlaySourceB(PlayerAudio.Instance.SE_ButtonClicked);
        switch (curRankAdjustStep)
        {
            case eRankAdjust.subject:

                //Forms display and close
                AfterSubject();

                break;

            case eRankAdjust.athlete:

                //Forms display and close
                AfterAthlete();

                break;

            case eRankAdjust.difficulty:

                //Forms display and close
                AfterDifficulty();
                break;

            case eRankAdjust.none:
                break;
            default:
                break;
        }
    }
    
    public void BtnFinancial()
    {
        PlayerAudio.Instance.PlaySourceA(PlayerAudio.Instance.SE_ButtonClicked);
        AdjustScore(4);
        btnFinancial.enabled = false; 
    }

    public void BtnEnvironment()
    {
        PlayerAudio.Instance.PlaySourceA(PlayerAudio.Instance.SE_ButtonClicked);
        AdjustScore(4);
        btnEnvironment.enabled = false;
    }

    public void BtnDisability()
    {
        PlayerAudio.Instance.PlaySourceA(PlayerAudio.Instance.SE_ButtonClicked);
        AdjustScore(4);
        btnDisability.enabled = false; 
    }

    public void BtnDisrupted()
    {
        PlayerAudio.Instance.PlaySourceA(PlayerAudio.Instance.SE_ButtonClicked);
        AdjustScore(4);
        btnDisrupted.enabled = false; 
    }

    private void AdjustScore(int score)
    {
        if (score >= 0)
        {
            PlayerData.Instance.AdjustScore += score;
        }
    }

    private void AfterSubject()
    {
        PlayerAudio.Instance.PlaySourceA(PlayerAudio.Instance.SE_WindowsOpen);
        CloseForm(eRankAdjust.subject);
        DisplayForm(eRankAdjust.athlete);
        curRankAdjustStep = eRankAdjust.athlete;
    }

    private void AfterAthlete()
    {
        PlayerAudio.Instance.PlaySourceA(PlayerAudio.Instance.SE_WindowsOpen);
        CloseForm(eRankAdjust.athlete);
        DisplayForm(eRankAdjust.difficulty);
        curRankAdjustStep = eRankAdjust.difficulty;
    }

    private void AfterDifficulty()
    {
        PlayerAudio.Instance.PlaySourceA(PlayerAudio.Instance.SE_WindowsOpen);
        CloseForm(eRankAdjust.difficulty);
        FlipControler.Instance.ChoiceBoard();
    }
}
