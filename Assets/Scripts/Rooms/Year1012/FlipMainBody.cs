using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FlipMainBody : MonoBehaviour
{
    public bool isKeepInBody;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    private void OnTriggerEnter(Collider other)
    {
        
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.tag == "RightHand")
        {
            isKeepInBody = false;
            //[DEBUG]
            //GameManager.Instance.txtDebug.text = "Out of body!";

        }
    }

    private void OnTriggerStay(Collider other)
    {
        if (other.tag == "RightHand")
        {
            isKeepInBody = true;

            //[DEBUG]
            //GameManager.Instance.txtDebug.text = "Keep in body!";
        }
    }
}
