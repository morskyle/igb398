using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TouchPoint : MonoBehaviour
{
    public bool isTouch;
    // Start is called before the first frame update
    void Start()
    {
        isTouch = false;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "LeftHand" || other.tag == "RightHand")
        {
            isTouch = true;

            //[DEBUG]GameManager.Instance.txtDebug.text = "Enter Touch point: " + gameObject.name;
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.tag == "LeftHand" || other.tag == "RightHand")
        {
            isTouch = false;
            //[DEBUG]GameManager.Instance.txtDebug.text = "Exit Touch point: " + gameObject.name;
        }
    }



}
