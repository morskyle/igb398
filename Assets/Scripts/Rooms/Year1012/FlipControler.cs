using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FlipControler : UnitySingleton<FlipControler>
{
    public GameObject choiceBoardYear1012;
    //public EnterPoint enterPoint;
    public GameObject leftPoint;
    public GameObject rightPoint;
    public GameObject mainBody;
    public GameObject book;
    public Image scoreBar;
    public Text scoreTxt;
    public Text timerTxt;

    public GameObject uiComponents;

    [SerializeField] private bool startGame;
    [SerializeField] private int flipScore;
    private bool isTouchLeft;
    private bool isTouchRight;
    private bool allowToPlay;

    // Start is called before the first frame update
    void Start()
    {
        allowToPlay = false;
        isTouchLeft = false;
        isTouchRight = false;
        flipScore = 0;
        scoreBar.fillAmount = 0;

        scoreTxt.text = "";
        //timerTxt.text = "";
    }

    // Update is called once per frame
    void Update()
    {
        //if (Input.GetKeyDown(KeyCode.Q))
        //{
        //    isTouchLeft = true;
        //    isTouchRight = true;
        //}

        //if (enterPoint.standInArea)
        //{
        //    allowToPlay = true;
            
        //}

        if (startGame)
        {
            if (mainBody.GetComponent<FlipMainBody>().isKeepInBody)
            {
                if (rightPoint.GetComponent<TouchPoint>().isTouch && !isTouchLeft)
                {
                    isTouchRight = true;
                }

                if (isTouchRight && leftPoint.GetComponent<TouchPoint>().isTouch)
                {
                    isTouchLeft = true;
                }

                if (isTouchRight && isTouchLeft)
                {
                    FlipBook();
                }
            }
            else
            {
                isTouchLeft = false;
                isTouchRight = false;
            }
        }


    }

    public void FlipBook()
    {
        PlayerAudio.Instance.PlaySourceA(PlayerAudio.Instance.SE_PageFlip);
        //flip
        book.GetComponent<AutoFlip>().FlipRightPage();

        flipScore += 1;
        scoreTxt.text = flipScore.ToString();
        scoreBar.fillAmount = (float)flipScore / 80;

        isTouchLeft = false;
        isTouchRight = false;
    }

    public void StartGame()
    {
        PlayerAudio.Instance.PauseBGM();

        leftPoint.SetActive(true); 
        rightPoint.SetActive(true);
        mainBody.SetActive(true);
        uiComponents.SetActive(true);

        TimerManager.Instance.year1012Timer.StartTimer();
        startGame = true;
    }
    public int GetScore()
    {
        return flipScore;
    }

    public void ResetFlipBookGame()
    {
        //Reset book page
        book.GetComponent<AutoFlip>().ControledBook.currentPage = 0;
        //Reset score
        flipScore = 0;
        //Reset data
        PlayerData.Instance.Year10to12Score = 0;

        //Reset UI 
        scoreTxt.text = flipScore.ToString();
        scoreBar.fillAmount = (float)flipScore / 80;

        //Player can play Flip Book again
        GameManager.Instance.playedFlipBook = false;
    }

    public void AfterGame()
    {
        Invoke("ContinueBGM", 1f);

        startGame = false;

        //Set game step to the next
        GameManager.Instance.SetGameStep(EGameStep.PathwayPassage);

        //Set bool player can NOT play game flip book again
        GameManager.Instance.playedFlipBook = true;

        //Change status: Is not playing game 
        GameManager.Instance.isGaming = false;

        //Record Score
        PlayerData.Instance.Year10to12Score = FlipControler.Instance.GetScore();

        //[DEBUG]GameManager.Instance.txtDebug.text = "Score: " + PlayerData.Instance.Year10to12Score;
        
        
        RankAdjustController.Instance.StartIntroduction();
        
        //DoorManager.Instance.door_Year1012_North.SetActive(false);
    }
   
    public void ChoiceBoard()
    {
        //Open Choice Board
        choiceBoardYear1012.SetActive(true);
        //Get score from player data
        choiceBoardYear1012.GetComponent<Year1012ChoiceBoard>().GetScoreData();
        //Display content
        choiceBoardYear1012.GetComponent<Year1012ChoiceBoard>().DisplayAccordingToScore();
    }
    private void ContinueBGM()
    {
        PlayerAudio.Instance.PlayBGM();
    }
}
