using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UI_GameIntroduction_FlipBook : MonoBehaviour
{
    public GameObject[] gameComponents;

    public void OnStartClicked()
    {
        StartGame();
        HideIntroduction();
    }

    public void StartGame()
    {
        foreach (GameObject gameComponent in gameComponents)
        {
            gameComponent.SetActive(true);
        }
    }

    public void HideIntroduction()
    {
        gameObject.SetActive(false);
    }
}
