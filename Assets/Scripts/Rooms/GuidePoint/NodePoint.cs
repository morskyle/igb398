using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NodePoint : GuidePoint
{
    public GameObject tafePoint;
    public GameObject uniPoint;
    public GameObject workPoint;

    protected override void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {
            PlaySoundEffect(PlayerAudio.Instance.SE_GuidePointTrigger);
            switch (PlayerData.Instance.TargetStage)
            {
                case ETargetStage.None:
                    break;
                case ETargetStage.TAFE:
                    tafePoint.SetActive(true);
                    break;
                case ETargetStage.University:
                    uniPoint.SetActive(true);
                    break;
                case ETargetStage.Work:
                    workPoint.SetActive(true);
                    break;
                default:
                    break;
            }
            gameObject.SetActive(false);
        }

        
    }
}
