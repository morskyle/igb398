using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SwitchPoint : GuidePoint
{
    public bool hideAfterTrigger = true;
    [Header("Next Point")]
    public bool hasNextPoint = false;
    public string triggerString = "Player";

    [Header("Next Item")]
    public bool isOpen = true;
    public GameObject[] controlledItems;
    
    protected override void OnTriggerEnter(Collider other)
    {
        if (other.tag == triggerString)
        {

            SwitchFunc();
            PlaySoundEffect(PlayerAudio.Instance.SE_GuidePointTrigger);

            //nextPoint.SetActive(true);

            if (hasNextPoint)
            {
                if (nextPoint)
                {
                    nextPoint.SetActive(hasNextPoint);
                }
            }

            if (hideTouchObj)
            {
                other.gameObject.SetActive(!hideTouchObj);
            }

            gameObject.SetActive(!hideAfterTrigger); 
        }
    }

    private void SwitchFunc()
    {
        foreach (GameObject controlledItem in controlledItems)
        {
            if (controlledItem)
            {
                controlledItem.SetActive(isOpen);
            }
        }
    }
}
