using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DoorPoint : GuidePoint
{
    public EDoor door;
    public bool hasNextPoint = false;
    protected override void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {
            PlayerAudio.Instance.PlaySourceB(PlayerAudio.Instance.SE_DoorOpen);
            DoorManager.Instance.CloseDoor(door);
            this.gameObject.SetActive(false);

            if (hasNextPoint)
            {
                if (nextPoint)
                {
                    nextPoint.SetActive(true);
                }
            }

            gameObject.SetActive(false);
        }
        
    }
}
