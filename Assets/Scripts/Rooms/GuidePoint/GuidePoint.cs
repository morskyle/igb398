using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum EGuidePointType
{
    Normal,
    PortalPoint,
    DoorPoint
}

public class GuidePoint : MonoBehaviour
{
    //public EGuidePointType pointType;
    public GameObject nextPoint;
    public bool hideTouchObj = false;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    protected virtual void OnTriggerEnter(Collider other)
    {
        PlaySoundEffect(PlayerAudio.Instance.SE_GuidePointTrigger);
        gameObject.SetActive(false);
        nextPoint.SetActive(true);
    }

    protected void PlaySoundEffect(AudioClip c)
    {

        PlayerAudio.Instance.PlaySourceA(c);
    }


}
