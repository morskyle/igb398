using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TouchTrigger : MonoBehaviour
{
    public bool isOpen;
    public bool hideAfterTrigger;
    public string colliderTag;
    public GameObject[] controledItems;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    protected virtual void OnTriggerEnter(Collider other)
    {
        if (other.tag == colliderTag)
        {
            if (controledItems.Length > 0)
            {
                foreach (GameObject controledItem in controledItems)
                {
                    controledItem.SetActive(true);
                }
            }

            gameObject.SetActive(hideAfterTrigger);
        }
    }
}
