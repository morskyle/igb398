using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum eBubbleType
{
    benefit,
    bad
}
public class Bubble : MonoBehaviour
{
    public eBubbleType type;
    private Rigidbody r;
    public bool canDestroy;
    public float moveSpeed = 4f;
    public IEnumerator autoBubbleDestory;

    // Start is called before the first frame update
    void Start()
    {
        PlayerAudio.Instance.PlaySourceC(PlayerAudio.Instance.SE_BubbleSpawn);
        canDestroy = true;
        autoBubbleDestory = AutoBubbleDestory();
        StartCoroutine(autoBubbleDestory);
        r = GetComponent<Rigidbody>();
    }

    // Update is called once per frame
    void Update()
    {
        Vector3 v = transform.position + Vector3.up.normalized * moveSpeed * Time.deltaTime;
        r.MovePosition(v);
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Sword")
        {
            if (canDestroy)
            {
                
                if (type == eBubbleType.benefit)
                {
                    PlayerAudio.Instance.PlaySourceA(PlayerAudio.Instance.SE_Bubble_Benefit);
                    UniGameController.Instance.AddScore(2);
                }
                else if(type == eBubbleType.bad)
                {
                    PlayerAudio.Instance.PlaySourceB(PlayerAudio.Instance.SE_Bubble_Bad);
                    UniGameController.Instance.AddScore(-2);
                }

                //cancel auto destory
                StopCoroutine(autoBubbleDestory);

                //destory
                Destroy(gameObject, 0.1f);

                UniGameController.Instance.RemoveBubbleFromList(this.gameObject);
            }
        }
    }

    private IEnumerator AutoBubbleDestory()
    {
        yield return new WaitForSeconds(8f);
        Destroy(gameObject);
    }

   
}
