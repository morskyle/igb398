using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BubbleSpawner : MonoBehaviour
{
    public GameObject[] bubbles;
    public IEnumerator spawner;

    // Start is called before the first frame update
    void Start()
    {
        spawner = Spawner();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void StartSpawn()
    {
        StartCoroutine(spawner);
    }

    public void StopSpawn()
    {
        StopCoroutine(spawner);
    }

    private IEnumerator Spawner()
    {
        while (true)
        {
            yield return new WaitForSeconds(Random.Range(3, 5));
            
            int type = Random.Range(0, bubbles.Length);
            Debug.Log("" + type);
            UniGameController.Instance.AddBubbleIntoList(Instantiate(bubbles[type], transform.position, new Quaternion(0, 0, 90, 1)));
        }


    }
}
