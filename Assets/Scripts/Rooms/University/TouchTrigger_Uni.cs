using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TouchTrigger_Uni : TouchTrigger
{
    
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    protected override void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {
            PlayerAudio.Instance.PlaySourceA(PlayerAudio.Instance.SE_GuidePointTrigger);
            UniGameController.Instance.StartGame();

            //Start to time
            TimerManager.Instance.universityTimer.StartTimer();

            //[Temporary] 
            this.gameObject.SetActive(false);
        }
        

    }

}
