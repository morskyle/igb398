using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UniGameController : UnitySingleton<UniGameController>
{
    [SerializeField] private int score = 0;
    public BubbleSpawner[] spawners;
    public List<GameObject> bubbles;
    public Text txtScore;
    public GameObject choiceBoardUni;
    public GameObject nextPoint;

    // Start is called before the first frame update
    void Start()
    {
        ShowScore();
    }

    private void ShowScore()
    {
        txtScore.text = "" + score;
    }

    //Record game score in PlayerData
    private void RecordScore()
    {
        PlayerData.Instance.TafeScore = score;
    }

    private void OpenChoiceBoard()
    {

    }

    //Remove Bubble from list
    public void AddBubbleIntoList(GameObject b)
    {
        bubbles.Add(b);
    }

    //Remove Bubble from list
    public void RemoveBubbleFromList(GameObject b)
    {
        bubbles.Remove(b);
    }

    //Clean all Bubbles
    public void RemoveAllBubbles()
    {
        foreach (GameObject bubble in bubbles)
        {
            Destroy(bubble);
        }
    }

    //Add score
    public void AddScore(int s)
    {
        score += s;
        ShowScore();
    }

    //Start game
    public void StartGame()
    {
        PlayerAudio.Instance.PauseBGM();
        Debug.Log("Start");
        foreach (BubbleSpawner spawner in spawners)
        {
            spawner.StartSpawn();
        }
    }

    //Stop game
    public void StopGame()
    {
        Invoke("ContinueBGM", 1f);
        foreach (BubbleSpawner spawner in spawners)
        {
            spawner.StopSpawn();
        }

        RemoveAllBubbles();
        RecordScore();
        nextPoint.SetActive(true);
    }
    
    //Reset game
    public void ResetGame()
    {
        //reset timer
        //reset trigger
    }
    private void ContinueBGM()
    {
        PlayerAudio.Instance.PlayBGM();
    }
}
