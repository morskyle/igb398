using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class Scholarship : MonoBehaviour
{
    public Button btnFinancial;
    public Button btnIndigenous;
    public Button btnRegional;
    public Button btnAcademic;

    public void OnFinancialClicked()
    {
        PlayerAudio.Instance.PlaySourceA(PlayerAudio.Instance.SE_Scholarship_Coin);
        btnFinancial.enabled = false;
    }

    public void OnIndigenousClicked()
    {
        PlayerAudio.Instance.PlaySourceA(PlayerAudio.Instance.SE_Scholarship_Coin);
        btnIndigenous.enabled = false;
    }

    public void OnRegionalClicked()
    {
        PlayerAudio.Instance.PlaySourceA(PlayerAudio.Instance.SE_Scholarship_Coin);
        btnRegional.enabled = false;
    }

    public void OnAcademicClicked()
    {
        PlayerAudio.Instance.PlaySourceA(PlayerAudio.Instance.SE_Scholarship_Coin);
        btnAcademic.enabled = false;
    }
}
