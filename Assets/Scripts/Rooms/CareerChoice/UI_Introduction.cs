using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UI_Introduction : MonoBehaviour
{
    public GameObject guidePoint;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void OpenPanel()
    {
        gameObject.SetActive(true);
    }

    public void HidePanel()
    {
        gameObject.SetActive(false);
        AfterHide();
    }

    public void AfterHide()
    {
        guidePoint.SetActive(true);
    }
}
