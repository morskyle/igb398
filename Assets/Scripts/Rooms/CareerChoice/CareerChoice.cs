using System.Collections;
using System.Collections.Generic;
using UnityEngine;




public class CareerChoice : MonoBehaviour
{
    public GameObject nextPoint;
    
    // Start is called before the first frame update
    void Start()
    {
       
    }
    
    // Update is called once per frame
    void Update()
    {
        
    }

    public void ChooseCareer(int career)
    {
        PlayerAudio.Instance.PlaySourceB(PlayerAudio.Instance.SE_ButtonClicked);
        PlayerData.Instance.Career = CareerData.Instance.IntToECareer[career];
        PlayerData.Instance.CareerName = CareerData.Instance.ECareerToString[PlayerData.Instance.Career];
        Debug.Log("career: " + CareerData.Instance.ECareerToString[PlayerData.Instance.Career]);
        Debug.Log("" + CareerData.Instance.careerData.Find(name => name.Jobname == PlayerData.Instance.CareerName).Age);
        AfterChoice();
    }
    
   

    private void AfterChoice()
    {
        
        nextPoint.SetActive(true);
        gameObject.SetActive(false);
        //DoorManager.Instance.CloseDoor(EDoor.door_CareerChoice_North);
    }

}
