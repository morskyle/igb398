using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum ETimer : int
{
    year1012Timer,
    universityTimer,
    tafeTimer,
    workTimer
}

public class TimerManager : UnitySingleton<TimerManager>
{
    public Year1012Timer year1012Timer;
    public UniversityTimer universityTimer;
    public TafeTimer tafeTimer; 
    public WorkTimer workTimer;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void StartTimer(ETimer timer)
    {
        switch (timer)
        {
            case ETimer.year1012Timer:
                year1012Timer.StartTimer();
                break;

            case ETimer.universityTimer:
                
                break;

            case ETimer.tafeTimer:
                tafeTimer.StartTimer();
                break;

            case ETimer.workTimer:
                workTimer.StartTimer();
                break;

            default:
                break;
        }
    }
}
