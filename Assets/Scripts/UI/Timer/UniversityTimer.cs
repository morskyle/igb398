using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UniversityTimer : GameTimer
{
    protected override void TimingOver()
    {
        base.TimingOver();

        UniGameController.Instance.StopGame();
    }
   
}
