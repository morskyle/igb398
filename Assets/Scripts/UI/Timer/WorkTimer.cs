using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WorkTimer : GameTimer
{
    protected override void TimingOver()
    {
        base.TimingOver();

        //TAFEGameController.Instance.StopGame();
        //TAFEGameController.Instance.AfterGame();
        WorkGameController.Instance.StopGame();
    }
}
