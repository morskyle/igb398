using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TafeTimer : GameTimer
{

    protected override void TimingOver()
    {
        base.TimingOver();

        TAFEGameController.Instance.StopGame();
        TAFEGameController.Instance.AfterGame();
    }
}
