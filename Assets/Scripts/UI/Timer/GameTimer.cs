using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameTimer : MonoBehaviour
{
    public int currentTime;//当前时间
    public int totalTime = 90;//总时间
    public Text timer;//在UI里显示时间
    private int minute;//分
    private int second;//秒
    public float duration = 1;
    private bool isStart = false;
    private IEnumerator startTime;

    // Start is called before the first frame update
    void Start()
    {
        startTime = StartTime();
        currentTime = totalTime;
    }

    public void StartTimer()
    {
        StartCoroutine(startTime);
    }

    public void StopTimer()
    {
        PlayerAudio.Instance.PlaySourceB(PlayerAudio.Instance.SE_TimesUp);
        StopCoroutine(startTime);
        currentTime = totalTime;
        TimerDisplay();
    }

    private IEnumerator StartTime()
    {
        WaitForSeconds waitForSeconds = new WaitForSeconds(duration);

        while (currentTime >= 0)
        {
            yield return waitForSeconds;
            currentTime--;

            //计时结束
            if (currentTime == 0)
            {
                TimingOver();
            }

            TimerDisplay();

        }

    }

    private void TimerDisplay()
    {
        PlayerAudio.Instance.PlaySourceC(PlayerAudio.Instance.SE_Timer);
        minute = currentTime / 60; //输出显示分
        second = currentTime % 60; //输出显示秒

        string length = minute.ToString();

        //如果秒大于10的时候，就输出格式为 00：00
        if (second >= 10)
        {
            timer.text = "0" + minute + ":" + second;
        }
        //如果秒小于10的时候，就输出格式为 00：00
        else if (second >= 0)
        {
            timer.text = "0" + minute + ":0" + second;
        }
    }

    protected virtual void TimingOver()
    {
        StopTimer();
        Debug.Log("计时结束！");
    }

}
