using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SimplePanelControl : MonoBehaviour
{
    public bool hasNextStuffs;
    public bool isOpen;
    public GameObject[] nextItems;
    public bool hideAfterClicked = true;

    // Start is called before the first frame update
    void Start()
    {
        PlayerAudio.Instance.PlaySourceA(PlayerAudio.Instance.SE_WindowsOpen);
    }

    public void OnClicked()
    {
        PlayerAudio.Instance.PlaySourceB(PlayerAudio.Instance.SE_ButtonClicked);

        if (hasNextStuffs)
        {
            if (nextItems.Length > 0)
            {
                foreach (GameObject nextItem in nextItems)
                {
                    nextItem.SetActive(isOpen);
                }
            }
        }
        
        gameObject.SetActive(!hideAfterClicked);
        //HideThePanel();
    }
    
    public void HideThePanel()
    {
        gameObject.SetActive(false);
    }
}
