using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
public class InGameMenu : MonoBehaviour
{
    public Slider slider_SE;
    public Slider slider_BGM;
    

    void Start()
    {
        this.gameObject.SetActive(false);  
    }
    private void OnEnable()
    {
        slider_SE.onValueChanged.AddListener(delegate { SEValueChangedHandler(); });
        slider_BGM.onValueChanged.AddListener(delegate { BGMValueChangedHandler(); });
    }

    private void OnDisable()
    {
        slider_SE.onValueChanged.RemoveAllListeners();
        slider_BGM.onValueChanged.RemoveAllListeners();
    }
    private void Update()
    {
        
    }

    public void OnPauseClicked()
    {
        PlayerAudio.Instance.PlaySourceA(PlayerAudio.Instance.SE_ButtonClicked);
        Time.timeScale = 0;
        this.gameObject.SetActive(false);
        GameManager.Instance.gameState.text = "Game Pause";
    }

    public void OnContinueClicked()
    {
        PlayerAudio.Instance.PlaySourceA(PlayerAudio.Instance.SE_ButtonClicked);
        Time.timeScale = 1;
        this.gameObject.SetActive(false);
        GameManager.Instance.gameState.text = "";
    }

    public void OnRestartClicked()
    {
        PlayerAudio.Instance.PlaySourceA(PlayerAudio.Instance.SE_ButtonClicked);
        Time.timeScale = 1;
        this.gameObject.SetActive(false);
        string scene = SceneManager.GetActiveScene().name;
        //Load it
        SceneManager.LoadScene(scene, LoadSceneMode.Single);
        GameManager.Instance.gameState.text = "";
    }

    public void OnQuitClicked()
    {
        PlayerAudio.Instance.PlaySourceA(PlayerAudio.Instance.SE_ButtonClicked);
        GameManager.Instance.gameState.text = "";
        Application.Quit();
        
    }

    private void SEValueChangedHandler()
    {
        PlayerAudio.Instance.SetSEVolume(slider_SE.value);
        
    }

    private void BGMValueChangedHandler()
    {
        PlayerAudio.Instance.SetBGMVolume(slider_BGM.value);
        
    }
}
