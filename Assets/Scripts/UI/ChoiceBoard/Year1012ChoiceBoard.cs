using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class Year1012ChoiceBoard : MonoBehaviour
{
    public Button btnTAFE;
    public Button btnUniversity;
    public Button btnWork;
    public Button btnInSchool;
    public FlipControler flipControler;
    public Text txtContent;
    public GameObject introduction;
    private int score;
    private int adjustScore;


    // Start is called before the first frame update
    void Start()
    {
        //GetScoreData();

        //DisplayAccordingToScore();

    }

    public void GetScoreData()
    {
        score = PlayerData.Instance.Year10to12Score;
        adjustScore = PlayerData.Instance.AdjustScore;
    }

    public void DisplayAccordingToScore()
    {
        int finalScore = score + adjustScore;

        //Final score can't less then zero
        if (finalScore < 0)
        {
            finalScore = 0;
        }

        if (finalScore < GameData.Instance.EnrollmentScore)
        {
            txtContent.text = "It looks like your score didn't reach the university's admissions line. But it doesn't matter, there are plenty of options here.";
            btnUniversity.enabled = false;
        }
        else if (finalScore >= GameData.Instance.EnrollmentScore)
        {
            string d = "University";
            txtContent.text = "Congratulations on a great score! We recommend you to choose the '" + d + "' option. But remember, this is not the only option. Your life should be decided directly by you.";
            btnUniversity.enabled = true;
        }
    }

    public void ChooseUniversity()
    {
        //DoorManager.Instance.door_Year1012_North.SetActive(false);
        //DoorManager.Instance.door_Pathway_University.SetActive(false);
        PlayerAudio.Instance.PlaySourceA(PlayerAudio.Instance.SE_ButtonClicked);
        PlayerData.Instance.TargetStage = ETargetStage.University;
        DisplayIntroduction();
        HideChoiceBoard();
    }

    public void ChooseTAFE()
    {
        //DoorManager.Instance.door_Year1012_North.SetActive(false);
        //DoorManager.Instance.door_Pathway_TAFE.SetActive(false);
        //DoorManager.Instance.door_TAFE_South.SetActive(false);
        PlayerAudio.Instance.PlaySourceA(PlayerAudio.Instance.SE_ButtonClicked);
        PlayerData.Instance.TargetStage = ETargetStage.TAFE;
        DisplayIntroduction();
        HideChoiceBoard();
    }

    public void ChooseWork()
    {
        //DoorManager.Instance.door_Year1012_North.SetActive(false);
        //DoorManager.Instance.door_Pathway_Work.SetActive(false);
        //DoorManager.Instance.door_Work_South.SetActive(false);
        PlayerAudio.Instance.PlaySourceA(PlayerAudio.Instance.SE_ButtonClicked);
        PlayerData.Instance.TargetStage = ETargetStage.Work;
        DisplayIntroduction();
        HideChoiceBoard();
    }

    public void InSchoolAgain()
    {
        //Reset the game Flip Book
        PlayerAudio.Instance.PlaySourceA(PlayerAudio.Instance.SE_ButtonClicked);
        PlayerData.Instance.TargetStage = ETargetStage.None;
        flipControler.ResetFlipBookGame();
        DisplayIntroduction();
        HideChoiceBoard();
    }

    private void DisplayIntroduction()
    {
        PlayerAudio.Instance.PlaySourceA(PlayerAudio.Instance.SE_WindowsOpen);
        introduction.SetActive(true);
    }
    private void HideChoiceBoard()
    {
        this.gameObject.SetActive(false);
    }
}
