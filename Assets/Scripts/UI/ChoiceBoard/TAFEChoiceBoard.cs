using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TAFEChoiceBoard : MonoBehaviour
{
    public GameObject player;
    public OVRPlayerController controller;
    public Transform university;
    public Transform careerSummary;

    // Start is called before the first frame update
    void Start()
    {
        PlayerAudio.Instance.PlaySourceB(PlayerAudio.Instance.SE_WindowsOpen);
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void BtnCareerSummary()
    {
        PlayerAudio.Instance.PlaySourceA(PlayerAudio.Instance.SE_ButtonClicked);
        MoveToCareerSummary();
    }

    public void BtnUniversity()
    {
        PlayerAudio.Instance.PlaySourceA(PlayerAudio.Instance.SE_ButtonClicked);
        MoveToUniversityPathway();
    }

    private void MoveToUniversityPathway()
    {
        university.gameObject.SetActive(true);

        controller.enabled = false;
        player.transform.position = university.position;
        controller.enabled = true;
        //player.position = new Vector3(-university.position.x, player.position.y, -university.position.z) ;
        //player.transform.position = new Vector3(player.transform.position.x - 14F, player.transform.position.y, player.transform.position.z);
        //player.transform.position = new Vector3(player.transform.position.x, player.transform.position.y, player.transform.position.z+11F);
        //player.transform.position = new Vector3(-73F, player.transform.position.y, 100);
        //player.transform.position = new Vector3(-70F, player.transform.position.y, player.transform.position.z);


        //player.position = new Vector3(player.position.x + 50f, player.position.y, player.position.z);


        //Vector3 startPos = player.transform.position;
        //Vector3 destPosition = university.position;
        //destPosition.y += player.height / 2.0f;

        //var character = player;
        //var characterTransform = character.transform;

        //var lerpPosition = Vector3.Lerp(startPos, destPosition, 1F);

        //characterTransform.position = lerpPosition;
    }

    private void MoveToCareerSummary()
    {
        careerSummary.gameObject.SetActive(true);

        controller.enabled = false;
        player.transform.position = careerSummary.position;
        controller.enabled = true;
    }
}
