using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Door : MonoBehaviour
{
    public GameObject doorObj;
    public GameObject doorSwitch;
    public GameObject popUpForm;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (doorSwitch.GetComponent<TouchPoint>().isTouch)
        {
            popUpForm.SetActive(true);
        }
    }

    public void BtnEnter()
    {
        popUpForm.SetActive(false);
        doorObj.SetActive(false);
    }

    public void BtnQuit()
    {
        popUpForm.SetActive(false);
    }
}
